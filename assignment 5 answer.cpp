#include <stdio.h>

float income (float x, float y)
{
	return x * y;
}
float cost (float y)
{
	return y * 3 + 500;
}
float profit (float x, float y)
{
	return income (x,y) - cost(y);
}
float highest (float h, float i, float j)
{
	if(h>=i && h>=j) return h;
	else if(i>=h && i>=j) return i;
	else return j;
}
float price (float h, float i, float j, float a, float b, float c)
{
if (h>=i && h>=j) return a;
else if (i>=h && i>=j) return b;
else return c;
}
int main ()
{
	float p,q,r,s,t,u,e,f,g,k;
	printf("Enter first proposed price: ");
	scanf("%f", &p);
	printf("Enter attendees: ");
	scanf("%f", &q);
	e = profit (p,q);
	
	printf("Enter second proposed price: ");
	scanf("%f", &r);
	printf("Enter attendees: ");
	scanf("%f", &s);
	f = profit (r,s);
	
	printf("Enter third proposed price: ");
	scanf("%f", &t);
	printf("Enter attendees: ");
	scanf("%f", &u);
	g = profit (t,u);
	
	k = highest (e,f,g);
	
	printf("Highest profit is: %.2f\n", k);
	printf("Price for the highest profit is: %.2f", price(e,f,g,p,r,t));
	return 0;
}
